# Problem Statement

Data on farmer's adoption of climate change mitigation measures, individual characteristics, risk attitudes and social influences in a region of Switzerland

Implementation Using Kubeflow.

## Data Discription

The data highlights which measures are taken by farmers to reduce greenhouse gas emissions and elicits climate change related perceptions, attitudes, self-efficacy, locus of control, social influences as well as risk preferences. Risk preferences, loss aversion and probability weighting were elicited by a multiple price list.

The data enables to understand the adoption of climate change mitigation measures in agriculture and associate it to farmers’ individual characteristics as well as farm structural characteristics.

Data Repo : http://hdl.handle.net/20.500.11850/383116

Research Paper : https://www.sciencedirect.com/science/article/pii/S2352340920303048

## About kubeflow Approach

There is two ways to easily start with kubeflow 

1. Kubeflow Onpremise (MiniKF)
2. Kubeflow on cloud

The kubeflow on premise approch requires us to have good system configurations to run experiments.
I have used Kubeflow on cloud approach using kubeflow SDK for this project.

## Repo Overview

kubeflow.ipynb : It is used as the main file to which all other files are connected.
modules : it is the directory where all our functions and configuration files are stored
config.py : It has all the configurations details to store and retrive data
get_data.py : This module has the code to read the dataset and store it in gcp bucket
process_data.py : This module has the code to preprocess out data so as to make it clean for training
train.py : this is were we are training our model 
requirements.txt : it holds all the dependencies which are to be installed to run our pipeline

## Set up the environment

1. Create an account in docker hub
2. Create a project in Google cloud platform
3. Create a new notebook instance
4. Enable Kubernetes and configure a kubeflow pipeline 

## How to replicate 

1. open the jupyter note book and open a terminal
2. git clone this repo
3. Open the kubeflow.ipynb and follow through the following:

        - Creating a docker image
        - Push the docker image to dockerhub
        - Define pipeline via components
        - Combine the components to form a complete pipeline
        - Compile the pipeline to generate the zip file with all the YAML files 
        - Open pipeline dashboard
        - Create the client
        - Create an experiment using the client
        - Create a run under the experiment


## Train Test and Monitor the Ml model

1. Training ML model
- Creating an Experiment
- Defining the Training container
- Running multiple ML model concurrently in containers
- Hyperparameter tuning (Katib)
- Running multiple run's in an experiment 

2. Testing ML model
- Defining the testing container
- Having multiple valuation metrics
- Comparing model and evaluation with other RUN's

3. Monitoring ML model 
- Mounting a volume
- defining training contatier for all training activites
- Defining Uploading contatier for uploading new versions
- Defining deploying container for deploying different versions to production
- Defining testing container for testing the use cases
- Defining cleaning container for cleaning up the code base and fine tuning
- Compile and execute this pipeline

## CI CD Model

All phases including Training ML model, Testing ML model and deploying ML model everything is containerized, which means if there is a new version of the model to be released, we can create a new Experiment test it with multiple runs and deploy the new version into production without any latency or downtime.


